﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 配列から始まるJSONをJsonUtilityでListに変換するヘルパー
/// http://forum.unity3d.com/threads/how-to-load-an-array-with-jsonutility.375735/
/// </summary>

public class JsonHelper 
{
   ///<summary>
   ///配列から始まるJSONデータを変換し、指定した型のListに直す
   /// </summary>
   /// <typeparam name="T">Listにしたいデータの型</typeparam>
   /// <param name="json">変換したいjson文字列</param>
   /// <returns>FromJsonで生成されたオブジェクトのList</returns>
   
    public static List<T> GetJsonArray<T>(string json)
    {
        string newJson = "{\"array\":" + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [Serializable]
    private class Wrapper<T>
    {
        //『#pragma～』は、プログラムの動きとは直接関係ない記述
        //(『この番号の警告を無視しろ』というコンパイラへの命令文)

#pragma warning disable 649;
        public List<T> array;
    }
}
