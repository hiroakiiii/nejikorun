﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour
{

    public Text highScoreLabel;

    public bool isDebug;//デバッグ用
    public Text userNameLabel;
    public Text inputUserName;

    /// <summary>
    /// 通信状態表示用の文字列
    /// </summary>
    public Text networkState;

    /// <summary>
    /// ランキング情報表示用のパネル
    /// </summary>
    public GameObject rankingTablePanel;

    /// <summary>
    /// 1位～3位の表示欄が入った配列
    /// </summary>
    public GameObject[] rankingRows;


    public void Start()
    {
        //デバッグ用
        //(インスペクタウィンドウ上の「isDebug」にチェックを入れると、ゲーム起動毎にデータが消える
        if(isDebug)
        {
            //isDebugにチェックが入っていたら、すべてのPrayerPrefsを削除
            PlayerPrefs.DeleteAll();
        }

        //ハイスコアを表示
        highScoreLabel.text = "High Score : " + PlayerPrefs.GetInt("HighScore") + "m";

        //登録しているユーザー名を表示
        UpdateUserName();

        //取得したConnectManagerから情報を引き出し、各種画面表示を行う
        StartCoroutine(InitWebUI());
    }

    public void OnStartButtonCliked()
    {
        Application.LoadLevel("Main");
    }

    ///<summary>
    ///「登録」ボタンを押したときの処理
    /// </summary>
    public void OnSubmitButtonClicked()
    {
        //正常に入力されていたらユーザー名を更新
        if(inputUserName.text.Length < 7)
        {
            PlayerPrefs.SetString("Mame", inputUserName.text);
            UpdateUserName();
        }
        else
        {
            Debug.Log("文字数:" + inputUserName.text.Length);
            userNameLabel.text = "6文字を超えています";
        }
    }


    ///<summary>
    ///ユーザー名の画面表示・更新処理
    /// </summary>
    void UpdateUserName()
    {
        string name = PlayerPrefs.GetString("Name");
        Debug.Log("TitleController: 現在登録されているユーザー名 /" + name);

        //初回起動時（プレイヤー名が登録されていない時）や未入力で登録時、ユーザー名を「Noname」に
        if (string.IsNullOrEmpty(name))
        {
            PlayerPrefs.SetString("Name", "Noname");
            name = "Noname";

        }

        //ユーザー名入力フォームに現在のユーザー名を入れておく
        userNameLabel.text = "ユーザー名:" + name;
    }

        ///<summary>
        ///サーバーへ接続し、取得した情報を元にUIを表示させるコルーチン
        /// </summary>
        /// <returns></returns>
        IEnumerator InitWebUI()
        {
            //ネットワークに接続、ランキング情報を取得
            yield return StartCoroutine(ConnectManager.FindMileage());
            //情報に合わせて各種画面表示
            ShowWebUI();
        }

        ///<summary>
        ///通信状態・ランキング情報をシーンへ表示するメソッド
        /// </summary>
        void ShowWebUI()
        {
            string baseText = "state:";

            //エラーならランキングを表示しない
            if(ConnectManager.IsError)
            {
                networkState.enabled = true;
                networkState.text = baseText + "通信エラーが発生しました";
            }
            else
            {
                networkState.enabled = true;
                networkState.text = baseText + "ランキング取得完了";
                WriteRankingTable();

            }
        }

        ///<summary>
        ///ランキング情報部分の表示処理
        /// </summary>
        void WriteRankingTable()
        {
            //各順位の位置を取得
            List<NejikoMileage> mileage = ConnectManager.Result;

            //表示用パネルをアクティブ化
            rankingTablePanel.SetActive(true);

            //各順位の情報を表示
            for(int i = 0; i < mileage.Count; i++)
            {
                ShowRankingRow(i);
                WriteRankingRow(rankingRows[i], mileage[i]);
            }
        }

        ///<summary>
        ///指定したindexのランキング情報をアクティブ化するメソッド
        /// </summary>
        /// <param name="index"></param>
        void ShowRankingRow(int index)
        {
            rankingRows[index].SetActive(true);
        }

        ///<summary>
        ///指定したcolorに指定したmileaeの情報を表示するメソッド
        /// </summary>
        /// <param name="row">走行記録の情報を表示する行</param>
        /// <param name="mileage">ランキングから取り出した走行記録</param>
        void WriteRankingRow(GameObject row, NejikoMileage mileage)
        {
            Text[] topics = row.GetComponentsInChildren<Text>();
            //Name.Scoreの表示内容をrecord通りに変更
            topics[1].text = mileage.name;
            topics[2].text = mileage.score.ToString();
        }
    
}
    // Start is called before the first frame update
  
